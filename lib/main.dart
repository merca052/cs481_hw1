//Name: Jesus Mercado
//Course: CS 481 (Mobile Programming)
//Semester: Fall 2020
//Assignment: Homework 1  (Hello World)
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

//This is the main widget which acts as our app, we technically
// put all other widgets on top of this widget in layers
// meaninf we put the code for all other
// widgets/actions of the app inside the Widgets declaration
class MyApp extends StatelessWidget {
  @override                   //every stateless widget requires an override
  Widget build(BuildContext context) {         //function called build
    return MaterialApp(                     //and returns the app itself
      //inside the declaration of the MaterialApp being returned,
      // we adjust the theme of the app making the brightness dark
      // and changing the primary color of the app
      theme: ThemeData(
        // Define the default brightness and colors.
        //changes background of app from white to dark grey(similar to this editors color)
        brightness: Brightness.dark,
        primaryColor: Colors.teal,   //Changes the appBar default color to teal

        // Define the default font family.
        fontFamily: 'Times New Roman',
      ),
      //To creates tabs in the appBAr we make the home screen of our app
      //tabbed and we use the default tab controller to control
      // the way the tabs function
      // (Note: a custom one may also be used if desired. Look up how)
      home: DefaultTabController(
        length: 2,     //We declare how many tabs we want
        child: Scaffold(    //We declare a scaffold to design our tabbed view
          appBar: AppBar(   //We declare the appBar here
            bottom: TabBar(    //at the bottom of the scaffold we add a tab bar
              //This will be at the bottom of the appBar under anything else in
              //the appBar declaration
              //Now we must declare what we want on the tab buttons
              // (i.e. text widget, icon widget, etc)
              //In this case we use icons for the tabs
              tabs: [
                Tab(icon: Icon(Icons.person)),    //Icon on the Name tab
                Tab(icon: Icon(Icons.record_voice_over)),//Icon on the Quote tab
              ],
            ),
            //Now that we have declared the tab bar we are almost done with the
            //appBar we just need to add a title to it which will display the
            //string "Hello World!!"
            //(Note: Since tab bar is set to the bottom of the appBar,
            // this will appear above the tab bar [above the icons])
            title: Center(
              //We set the title attribute to a Center widget and within this
              //Centered widget:
              child: Text(
                //we add the string
                'Hello World!!',
                // we style it using the "style:" attribute.
                // We use the TextStyles widget to change:
                style: TextStyle(
                  // font to bold
                    fontWeight: FontWeight.bold,
                    //the font size
                    fontSize: 25.0),
              ),
            ),
          ),
          //Now we have completed the appBar and we may close its declaration.
          //We should see a title with tabs underneath it and blank pages when
          //we navigate from tab to tab.
          //Below is where we will specify what content to put in each tab
          //In the body of the Scaffold, we declare the tab bar view
          body: TabBarView(
            //We add a list of widgets called children which will only
            //take in as many parameters as there are tabs. So in our case
            //only two widget children accepted. Since this is the case using
            //Text(),Icon(),IMage() would only allow us to add one widget per
            //tab.
            children: [
              //To work around this we create a Column widget
              //This will be everything displayed in the first tab
              //Tab 1:
              Column(
                //declare layout attributes for the whole widget (Column)
                mainAxisAlignment: MainAxisAlignment.center, //center vertically

                //In order to get multiple lines of text or widgets in this tab
                //We will declare children for the column widget which acts as
                //our tab's body
                //In this tab we want an image of myself with my Name directly
                // under the image and directly under my name, my grad year.
                children: [
                  //Here we declare one of the child widgets of this tab is
                  // an Image of myself (uploaded to the assets folder and
                  //rendered using Image.assets with the path to the image and
                  //sizing specifications)
                  //Child 1:
                  Image.asset('assets/IMG_1389.jpeg', width: 500.0, height: 500.0),

                  //Row widget for displaying name under image of me
                  //Since i desire the my named to be displayed using two
                  // text widgets on the same horizontal line, I use the row
                  // widget
                  //Child 2:
                  Row(
                    //spaced evenly on the horizontal axis of app screen
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                    children: [
                      //and declare these text widgets as it's children.
                      //This allows us to work around the same one widget per child
                      //constraint of the children attribute
                      Text('    Name:', style: TextStyle(color: Colors.cyanAccent, fontWeight: FontWeight.bold, fontSize: 20.0,),),
                      Text('Jesus Mercado', style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold) ),  //and were done with this part then we simply add an image of my favorite quote using assests and we are done
                    ],
                  ),
                  //Now the last text widgets we need are for my grad year
                  //which will go under the line for my name
                  //Child 3:
                  // (follows same concept/implementation as above row widget)
                  Row(
                    //aligning the axis
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                    //declaring children Text widgets
                    children: [
                      Text('Graduation:', style: TextStyle(color: Colors.cyanAccent, fontWeight: FontWeight.bold, fontSize: 20.0,),),
                      Text('Spring 2022 possibly Fall 2021', style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold)),
                    ],
                  ),
                  //Now we have declared all the widgets we need for this tab.
                  //So we close the children's list of the 1st column widget
                  // of the tab bar view's children
                ],
              ),
              //Now we must declare what we want on the second tab/ second
              // child of the tab bar view's children list. Since we only wish
              // to add a single Image widget to this tab, we can just directly
              // declare this single widget as a child of the tab bar view.
              //Tab 2:
              Image.asset('assets/quote.jpg', width: 400.0, height: 400.0),
            ],
          ),
        ),
      ),
    );
  }
}